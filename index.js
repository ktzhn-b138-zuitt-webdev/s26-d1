let http = require("http");

// HTTP Status Codes: https://developer.mozilla.org/en-US/docs/Web/HTTP/Status

http.createServer(function (request, response) {
	response.writeHead(200, {'Content-Type': 'text/plain'});
	response.end('Hello World!');
}).listen(4000);

console.log('Server is running at localhost:4000');